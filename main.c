#define NOTERM

// Standard includes
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

// Simplelink includes
#include "simplelink.h"

// Driverlib includes
#include "rom.h"
#include "rom_map.h"
#include "hw_gpio.h"
#include "hw_memmap.h"
#include "hw_common_reg.h"
#include "hw_uart.h"
#include "hw_types.h"
#include "hw_ints.h"
#include "uart.h"
#include "udma.h"
#include "interrupt.h"
#include "utils.h"
#include "prcm.h"
#include "gpio.h"
#include "utils.h"
#include "timer.h"

// Common interface includes
#include "udma_if.h"
#include "gpio_if.h"

#include "pinmux.h"

//Common interface includes
#include "systick_if.h"
#include "common.h"

#include "circ_buff.h"

//*****************************************************************************
//                          MACROS
//*****************************************************************************
#define PREAMBLE            1        /* Preamble value 0- short, 1- long */
#define CPU_CYCLES_1MSEC (80*1000)

#define PAYLOAD_SIZE (BUFFER_SIZE - 8)

#define CHANNEL_NUMBER 2
#define POWER_LEVEL_TONE    15
#define PREAMBLE            1

#define TRANSCEIVER_FRAME_MAX_LENGTH 1476
#define TRANSCEIVER_FRAME_MIN_LENGTH 18

#define WLAN_FCS 4

// This is the time we delay inside the A0 timer interrupt handler.
#define TIMER_DELAY_uS 200000

#define UART_BAUD_RATE  921600

#define DUPLEX

typedef struct {
	_i8 FRAME_CONTROL[2];
	_i8 DURATION_ID[2];
	_i8 DESTINATION_MAC_ADDR[BSSID_LEN_MAX];
	_i8 BSSID_MAC_ADDR[BSSID_LEN_MAX];
	_i16 packet_length;
} TransceiverFrameHeader;

typedef struct {

	unsigned char rate;
	unsigned char channel;
	char rssi;
	unsigned char padding;
	unsigned long timestamp;
} TransceiverRxOverHead_t;

// Application specific status/error codes
typedef enum {
	// Choosing -0x7D0 to avoid overlap w/ host-driver's error codes
	TX_CONTINUOUS_FAILED = -0x7D0,
	RX_STATISTICS_FAILED = TX_CONTINUOUS_FAILED - 1,
	DEVICE_NOT_IN_STATION_MODE = RX_STATISTICS_FAILED - 1,

	STATUS_CODE_MAX = -0xBB8
} e_AppStatusCodes;

#define UART_UDMA_TRANSFER_SIZE 1024
#define RX_PACKET_SIZE (UART_UDMA_TRANSFER_SIZE + sizeof(TransceiverRxOverHead_t) + sizeof(TransceiverFrameHeader) + WLAN_FCS)
#define NUMBER_OF_PACKETS_PER_RX_BUFFER 30
#define RX_BUFFER_SIZE NUMBER_OF_PACKETS_PER_RX_BUFFER*RX_PACKET_SIZE

//*****************************************************************************
//                 GLOBAL VARIABLES -- Start
//*****************************************************************************

volatile int g_iCounter = 0;

volatile static tBoolean bRxDone;

volatile unsigned long g_ulStatus = 0; //SimpleLink Status
unsigned long g_ulGatewayIP = 0; //Network Gateway IP address
unsigned char g_ucConnectionSSID[SSID_LEN_MAX + 1]; //Connection SSID
unsigned char g_ucConnectionBSSID[BSSID_LEN_MAX]; //Connection BSSID

volatile _i16 socket_handle = 0;

volatile unsigned int packet_contents_length = 0;

volatile static tBoolean bTXdone = true;

volatile tBoolean hasDataToSend = false;

// The transmit and receive buffers used for the UART transfers.  There is one
// transmit buffer and a pair of receive ping-pong buffers.
unsigned char g_ucTxBuf[UART_UDMA_TRANSFER_SIZE
		+ sizeof(TransceiverRxOverHead_t) + sizeof(TransceiverFrameHeader)
		+ WLAN_FCS];
unsigned char g_ucRxBufA[UART_UDMA_TRANSFER_SIZE
		+ sizeof(TransceiverFrameHeader) + WLAN_FCS];
unsigned char g_ucRxBufB[UART_UDMA_TRANSFER_SIZE
		+ sizeof(TransceiverFrameHeader) + WLAN_FCS];
unsigned char g_uartFifoBuf[sizeof(TransceiverFrameHeader) + WLAN_FCS + 8];

tCircularBuffer *pRxBuffer;

unsigned char (*sendBuffer)[sizeof(g_ucRxBufA)];

long sentBytes;

TransceiverFrameHeader transceiverFrame_header = { 0x88,		// FRAME CONTROL
		0x02, 0x2C, 0x00,							// DURATION ID
		0x00, 0x23, 0x75, 0x55, 0x55, 0x55, 	// DESTINATION MAC ADDRESS
		0x00, 0x22, 0x75, 0x55, 0x55, 0x55 	// BSSID MAC ADDRESS
		};

#if defined(ccs)
extern void (* const g_pfnVectors[])(void);
#endif
#if defined(ewarm)
extern uVectorEntry __vector_table;
#endif
//*****************************************************************************
//                 GLOBAL VARIABLES -- End
//*****************************************************************************

//****************************************************************************
//                      LOCAL FUNCTION PROTOTYPES
//****************************************************************************
static void InitializeAppVariables() {
	g_ulStatus = 0;
	g_ulGatewayIP = 0;
	memset(g_ucConnectionSSID, 0, sizeof(g_ucConnectionSSID));
	memset(g_ucConnectionBSSID, 0, sizeof(g_ucConnectionBSSID));

}

void SimpleLinkWlanEventHandler(SlWlanEvent_t *pWlanEvent) {
	switch (pWlanEvent->Event) {
	case SL_WLAN_CONNECT_EVENT: {
		SET_STATUS_BIT(g_ulStatus, STATUS_BIT_CONNECTION);

		// Copy new connection SSID and BSSID to global parameters
		memcpy(g_ucConnectionSSID,
				pWlanEvent->EventData.STAandP2PModeWlanConnected.ssid_name,
				pWlanEvent->EventData.STAandP2PModeWlanConnected.ssid_len);
		memcpy(g_ucConnectionBSSID,
				pWlanEvent->EventData.STAandP2PModeWlanConnected.bssid,
				SL_BSSID_LENGTH);
	}
		break;

	case SL_WLAN_DISCONNECT_EVENT: {
		CLR_STATUS_BIT(g_ulStatus, STATUS_BIT_CONNECTION);
		CLR_STATUS_BIT(g_ulStatus, STATUS_BIT_IP_AQUIRED);

		memset(g_ucConnectionSSID, 0, sizeof(g_ucConnectionSSID));
		memset(g_ucConnectionBSSID, 0, sizeof(g_ucConnectionBSSID));
	}
		break;

	default: {

	}
		break;
	}
}

void SimpleLinkNetAppEventHandler(SlNetAppEvent_t *pNetAppEvent) {
	switch (pNetAppEvent->Event) {
	case SL_NETAPP_IPV4_IPACQUIRED_EVENT: {
		SlIpV4AcquiredAsync_t *pEventData = NULL;

		SET_STATUS_BIT(g_ulStatus, STATUS_BIT_IP_AQUIRED);

		//Ip Acquired Event Data
		pEventData = &pNetAppEvent->EventData.ipAcquiredV4;

		//Gateway IP address
		g_ulGatewayIP = pEventData->gateway;
	}
		break;

	default: {
	}
		break;
	}
}

void SimpleLinkHttpServerCallback(SlHttpServerEvent_t *pHttpEvent,
		SlHttpServerResponse_t *pHttpResponse) {
}

void SimpleLinkGeneralEventHandler(SlDeviceEvent_t *pDevEvent) {
}

void SimpleLinkSockEventHandler(SlSockEvent_t *pSock) {
}

static long ConfigureSimpleLinkToDefaultState() {
	SlVersionFull ver = { 0 };
	_WlanRxFilterOperationCommandBuff_t RxFilterIdMask = { 0 };

	unsigned char ucVal = 1;
	unsigned char ucConfigOpt = 0;
	unsigned char ucConfigLen = 0;
	unsigned char ucPower = 0;

	long lRetVal = -1;
	long lMode = -1;

	lMode = sl_Start(0, 0, 0);
	ASSERT_ON_ERROR(lMode);

	// If the device is not in station-mode, try configuring it in station-mode
	if (ROLE_STA != lMode) {
		if (ROLE_AP == lMode) {
			// If the device is in AP mode, we need to wait for this event
			// before doing anything
			while (!IS_IP_ACQUIRED(g_ulStatus)) {
			}
		}

		// Switch to STA role and restart
		lRetVal = sl_WlanSetMode(ROLE_STA);
		ASSERT_ON_ERROR(lRetVal);

		lRetVal = sl_Stop(0xFF);
		ASSERT_ON_ERROR(lRetVal);

		lRetVal = sl_Start(0, 0, 0);
		ASSERT_ON_ERROR(lRetVal);

		// Check if the device is in station again
		if (ROLE_STA != lRetVal) {
			// We don't want to proceed if the device is not coming up in STA-mode
			return DEVICE_NOT_IN_STATION_MODE;
		}
	}

	// Get the device's version-information
	ucConfigOpt = SL_DEVICE_GENERAL_VERSION;
	ucConfigLen = sizeof(ver);
	lRetVal = sl_DevGet(SL_DEVICE_GENERAL_CONFIGURATION, &ucConfigOpt,
			&ucConfigLen, (unsigned char *) (&ver));
	ASSERT_ON_ERROR(lRetVal);

	// Set connection policy to Auto + SmartConfig
	//      (Device's default connection policy)
	lRetVal = sl_WlanPolicySet(SL_POLICY_CONNECTION,
			SL_CONNECTION_POLICY(1, 0, 0, 0, 1), NULL, 0);
	ASSERT_ON_ERROR(lRetVal);

	// Remove all profiles
	lRetVal = sl_WlanProfileDel(0xFF);
	ASSERT_ON_ERROR(lRetVal);

	//
	// Device in station-mode. Disconnect previous connection if any
	// The function returns 0 if 'Disconnected done', negative number if already
	// disconnected Wait for 'disconnection' event if 0 is returned, Ignore
	// other return-codes
	//
	lRetVal = sl_WlanDisconnect();
	if (0 == lRetVal) {
		// Wait
		while (IS_CONNECTED(g_ulStatus)) {

		}
	}

	// Enable DHCP client
	lRetVal = sl_NetCfgSet(SL_IPV4_STA_P2P_CL_DHCP_ENABLE, 1, 1, &ucVal);
	ASSERT_ON_ERROR(lRetVal);

	// Disable scan
	ucConfigOpt = SL_SCAN_POLICY(0);
	lRetVal = sl_WlanPolicySet(SL_POLICY_SCAN, ucConfigOpt, NULL, 0);
	ASSERT_ON_ERROR(lRetVal);

	// Set Tx power level for station mode
	// Number between 0-15, as dB offset from max power - 0 will set max power
	ucPower = 0;
	lRetVal = sl_WlanSet(SL_WLAN_CFG_GENERAL_PARAM_ID,
	WLAN_GENERAL_PARAM_OPT_STA_TX_POWER, 1, (unsigned char *) &ucPower);
	ASSERT_ON_ERROR(lRetVal);

	// Set PM policy to normal
	lRetVal = sl_WlanPolicySet(SL_POLICY_PM, SL_NORMAL_POLICY, NULL, 0);
	ASSERT_ON_ERROR(lRetVal);

	// Unregister mDNS services
	lRetVal = sl_NetAppMDNSUnRegisterService(0, 0);
	ASSERT_ON_ERROR(lRetVal);

	// Remove  all 64 filters (8*8)
	memset(RxFilterIdMask.FilterIdMask, 0xFF, 8);
	lRetVal = sl_WlanRxFilterSet(SL_REMOVE_RX_FILTER, (_u8 *) &RxFilterIdMask,
			sizeof(_WlanRxFilterOperationCommandBuff_t));
	ASSERT_ON_ERROR(lRetVal);

	lRetVal = sl_Stop(SL_STOP_TIMEOUT);
	ASSERT_ON_ERROR(lRetVal);

	InitializeAppVariables();

	return lRetVal; // Success
}

static void BoardInit(void) {
	/* In case of TI-RTOS vector table is initialize by OS itself */
#ifndef USE_TIRTOS
	//
	// Set vector table base
	//
#if defined(ccs)
	MAP_IntVTableBaseSet((unsigned long) &g_pfnVectors[0]);
#endif
#if defined(ewarm)
	MAP_IntVTableBaseSet((unsigned long)&__vector_table);
#endif
#endif
	//
	// Enable Processor
	//
	MAP_IntMasterEnable();
	MAP_IntEnable(FAULT_SYSTICK);

	PRCMCC3200MCUInit();
}

static int CreateFilters() {
	SlrxFilterID_t FilterID = 0;
	SlrxFilterRuleType_t RuleType;
	SlrxFilterFlags_t FilterFlags;
	SlrxFilterRule_t Rule;
	SlrxFilterTrigger_t Trigger;
	SlrxFilterAction_t Action;

	unsigned char ucBSSIDMacAddress[BSSID_LEN_MAX] = { 0x00, 0x22, 0x75, 0x55,
			0x55, 0x55 };
	unsigned char ucBSSIDMask[BSSID_LEN_MAX] = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
			0xFF };

	//
	// define filter as parent
	//
	Trigger.ParentFilterID = NULL; // NULL indicates that it is the root filter node

	//
	// no trigger to activate the filter.
	//
	Trigger.Trigger = NO_TRIGGER;

	//
	//connection state and role
	//
	Trigger.TriggerArgConnectionState.IntRepresentation =
	RX_FILTER_CONNECTION_STATE_STA_NOT_CONNECTED;
	Trigger.TriggerArgRoleStatus.IntRepresentation = RX_FILTER_ROLE_PROMISCUOUS;

	//
	// header and Combination types are only supported. Combination for logical
	// AND/OR between two filters
	//
	RuleType = HEADER;
	Rule.HeaderType.RuleHeaderfield = BSSID_FIELD;
	memcpy(
			Rule.HeaderType.RuleHeaderArgsAndMask.RuleHeaderArgs.RxFilterDB6BytesRuleArgs[0],
			ucBSSIDMacAddress, BSSID_LEN_MAX);
	memcpy(Rule.HeaderType.RuleHeaderArgsAndMask.RuleHeaderArgsMask,
			ucBSSIDMask, BSSID_LEN_MAX);
	Rule.HeaderType.RuleCompareFunc = COMPARE_FUNC_NOT_EQUAL_TO;

	//
	// Action
	//
	Action.ActionType.IntRepresentation = RX_FILTER_ACTION_DROP;
	FilterFlags.IntRepresentation = RX_FILTER_BINARY;
	long retVal = sl_WlanRxFilterAdd(RuleType, FilterFlags, &Rule, &Trigger,
			&Action, &FilterID);

	_WlanRxFilterOperationCommandBuff_t RxFilterIDMask;

	memset(RxFilterIDMask.FilterIdMask, 0xFF, 8);
	retVal = sl_WlanRxFilterSet(SL_ENABLE_DISABLE_RX_FILTER,
			(unsigned char *) &RxFilterIDMask,
			sizeof(_WlanRxFilterOperationCommandBuff_t));

	return 0;
}

static void TimerA0InterruptHandler(void) {
//	HWREG(GPIOA0_BASE + (GPIO_O_GPIO_DATA + (0x01 << 2))) = ~HWREG(GPIOA0_BASE + (GPIO_O_GPIO_DATA + (0x01 << 2)));

	//GPIO_IF_LedToggle(MCU_GREEN_LED_GPIO);

	if (uDMAChannelModeGet(
	UDMA_UART_RX_CHANNEL | UDMA_PRI_SELECT) == UDMA_MODE_PINGPONG) {

		if ((UART_UDMA_TRANSFER_SIZE
				- uDMAChannelSizeGet(UDMA_UART_RX_CHANNEL | UDMA_PRI_SELECT))) {
			uDMAChannelDisable(UDMA_UART_RX_CHANNEL | UDMA_PRI_SELECT);
			transceiverFrame_header.packet_length = UART_UDMA_TRANSFER_SIZE
					- uDMAChannelSizeGet(
					UDMA_UART_RX_CHANNEL | UDMA_PRI_SELECT);

			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			if (UARTCharsAvail(UART_BASE_PTR)) {
				int counter = 0;
				while (UARTCharsAvail(UART_BASE_PTR) && counter++ < 8)
					g_ucRxBufA[sizeof(TransceiverFrameHeader)
							+ transceiverFrame_header.packet_length++] =
							UARTCharGetNonBlocking(UART_BASE_PTR);
			}
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

			memcpy(
					&(g_ucRxBufA[sizeof(transceiverFrame_header)
							- sizeof(transceiverFrame_header.packet_length)]),
					&(transceiverFrame_header.packet_length),
					sizeof(transceiverFrame_header.packet_length));
			sendBuffer = g_ucRxBufA;
			hasDataToSend = true;
//			unsigned int frameLength = sizeof(transceiverFrame_header)+ transceiverFrame_header.packet_length;
//			HWREG(GPIOA3_BASE + (GPIO_O_GPIO_DATA + (0x10 << 2))) |= 0x10;
//			while(sl_Send(socket_handle, g_ucRxBufA, frameLength + WLAN_FCS,
//					SL_RAW_RF_TX_PARAMS(CHANNEL_NUMBER, RATE_54M, POWER_LEVEL_TONE, PREAMBLE)) != (frameLength + WLAN_FCS));
//			HWREG(GPIOA3_BASE + (GPIO_O_GPIO_DATA + (0x10 << 2))) &= ~0x10;
			uDMAChannelControlSet(UDMA_UART_RX_CHANNEL | UDMA_PRI_SELECT,
			UDMA_SIZE_8 | UDMA_SRC_INC_NONE | UDMA_DST_INC_8 | UDMA_ARB_8);
			uDMAChannelAttributeEnable(UDMA_UART_RX_CHANNEL | UDMA_PRI_SELECT,
			UDMA_ATTR_USEBURST);
			uDMAChannelTransferSet(UDMA_UART_RX_CHANNEL | UDMA_PRI_SELECT,
			UDMA_MODE_PINGPONG, (void *) (UART_BASE_PTR + UART_O_DR),
					&(g_ucRxBufA[sizeof(TransceiverFrameHeader)]),
					UART_UDMA_TRANSFER_SIZE);
			uDMAChannelEnable(UDMA_UART_RX_CHANNEL | UDMA_PRI_SELECT);
		}

	}
	if (uDMAChannelModeGet(
			UDMA_UART_RX_CHANNEL | UDMA_ALT_SELECT) == UDMA_MODE_PINGPONG) {

		if (UART_UDMA_TRANSFER_SIZE
				- uDMAChannelSizeGet(UDMA_UART_RX_CHANNEL | UDMA_ALT_SELECT)) {
			uDMAChannelDisable(UDMA_UART_RX_CHANNEL | UDMA_ALT_SELECT);
			transceiverFrame_header.packet_length = UART_UDMA_TRANSFER_SIZE
					- uDMAChannelSizeGet(
					UDMA_UART_RX_CHANNEL | UDMA_ALT_SELECT);

			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			if (UARTCharsAvail(UART_BASE_PTR)) {
				int counter = 0;
				while (UARTCharsAvail(UART_BASE_PTR) && counter < 8)
					g_ucRxBufB[sizeof(TransceiverFrameHeader)
							+ transceiverFrame_header.packet_length++] =
							UARTCharGetNonBlocking(UART_BASE_PTR);
			}
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

			memcpy(
					&(g_ucRxBufB[sizeof(transceiverFrame_header)
							- sizeof(transceiverFrame_header.packet_length)]),
					&(transceiverFrame_header.packet_length),
					sizeof(transceiverFrame_header.packet_length));
			sendBuffer = g_ucRxBufB;
			hasDataToSend = true;
//			unsigned int frameLength = sizeof(transceiverFrame_header)+ transceiverFrame_header.packet_length;
//			HWREG(GPIOA3_BASE + (GPIO_O_GPIO_DATA + (0x10 << 2))) |= 0x10;
//			while(sl_Send(socket_handle, g_ucRxBufB, frameLength + WLAN_FCS,
//					SL_RAW_RF_TX_PARAMS(CHANNEL_NUMBER, RATE_54M, POWER_LEVEL_TONE, PREAMBLE)) != (frameLength + WLAN_FCS));
//			HWREG(GPIOA3_BASE + (GPIO_O_GPIO_DATA + (0x10 << 2))) &= ~0x10;
			uDMAChannelControlSet(UDMA_UART_RX_CHANNEL | UDMA_ALT_SELECT,
			UDMA_SIZE_8 | UDMA_SRC_INC_NONE | UDMA_DST_INC_8 | UDMA_ARB_8);
			uDMAChannelAttributeEnable(UDMA_UART_RX_CHANNEL | UDMA_ALT_SELECT,
			UDMA_ATTR_USEBURST);
			uDMAChannelTransferSet(UDMA_UART_RX_CHANNEL | UDMA_ALT_SELECT,
			UDMA_MODE_PINGPONG, (void *) (UART_BASE_PTR + UART_O_DR),
					&(g_ucRxBufB[sizeof(TransceiverFrameHeader)]),
					UART_UDMA_TRANSFER_SIZE);
			uDMAChannelEnable(UDMA_UART_RX_CHANNEL | UDMA_ALT_SELECT);
		}
	}

	if (UARTCharsAvail(UART_BASE_PTR)) {
		transceiverFrame_header.packet_length = 0;

		while (UARTCharsAvail(UART_BASE_PTR)
				&& transceiverFrame_header.packet_length < 8)
			g_uartFifoBuf[sizeof(TransceiverFrameHeader)
					+ transceiverFrame_header.packet_length++] =
					UARTCharGetNonBlocking(UART_BASE_PTR);

		memcpy(
				&(g_uartFifoBuf[sizeof(transceiverFrame_header)
						- sizeof(transceiverFrame_header.packet_length)]),
				&(transceiverFrame_header.packet_length),
				sizeof(transceiverFrame_header.packet_length));

		sendBuffer = g_uartFifoBuf;
		hasDataToSend = true;
//		HWREG(GPIOA3_BASE + (GPIO_O_GPIO_DATA + (0x10 << 2))) |= 0x10;
//		while(sl_Send(socket_handle, g_uartFifoBuf, sizeof(transceiverFrame_header) + transceiverFrame_header.packet_length + WLAN_FCS,
//							SL_RAW_RF_TX_PARAMS(CHANNEL_NUMBER, RATE_54M, POWER_LEVEL_TONE, PREAMBLE)) != (sizeof(transceiverFrame_header) + transceiverFrame_header.packet_length + WLAN_FCS));
//		HWREG(GPIOA3_BASE + (GPIO_O_GPIO_DATA + (0x10 << 2))) &= ~0x10;
	}

//	HWREG(GPIOA0_BASE + (GPIO_O_GPIO_DATA + (0x01 << 2))) = ~HWREG(GPIOA0_BASE + (GPIO_O_GPIO_DATA + (0x01 << 2)));

//	uDMAChannelEnable(UDMA_UART_RX_CHANNEL | UDMA_PRI_SELECT);
//	uDMAChannelEnable(UDMA_UART_RX_CHANNEL | UDMA_ALT_SELECT);// REDUNDANCIA ?!
	UARTDMAEnable(UART_BASE_PTR, UART_DMA_RX);
	MAP_TimerIntClear(TIMERA0_BASE, 1);
}

static void uDMAErrorHandler(void) {
	if (uDMAErrorStatusGet()) {
//		HWREG(GPIOA1_BASE + (GPIO_O_GPIO_DATA + (0x04 << 2))) |= 0x04;
		uDMAErrorStatusClear();
	}
}

static void UARTIntHandler() {
//	HWREG(GPIOA2_BASE + (GPIO_O_GPIO_DATA + (0x02 << 2))) = ~HWREG(GPIOA2_BASE + (GPIO_O_GPIO_DATA + (0x02 << 2)));
	unsigned long interruptStatus = MAP_UARTIntStatus(UART_BASE_PTR,
	true);

	if (UARTRxErrorGet(UART_BASE_PTR)) {
//		HWREG(GPIOA1_BASE + (GPIO_O_GPIO_DATA + (0x02 << 2))) |= 0x02;// Turn ON Red LED to sign ERROR
		UARTIntClear(UART_BASE_PTR,
		UART_INT_OE | UART_INT_BE | UART_INT_FE | UART_INT_PE);
		UARTRxErrorClear(UART_BASE_PTR);
	}

	if (interruptStatus & UART_INT_DMATX) {
//		UARTDMADisable(UART_BASE_PTR, UART_DMA_TX);
//		bTXdone = true;

		if (!IsBufferEmpty(pRxBuffer)) {

			TransceiverFrameHeader *frame_header = NULL;

			frame_header =
					(TransceiverFrameHeader *) &(pRxBuffer->pucReadPtr[sizeof(TransceiverRxOverHead_t)]);

			unsigned char* contents =
					&(pRxBuffer->pucReadPtr[sizeof(TransceiverRxOverHead_t)
							+ sizeof(TransceiverFrameHeader)]);

			UDMASetupTransfer(UDMA_UART_TX_CHANNEL | UDMA_PRI_SELECT,
			UDMA_MODE_BASIC, frame_header->packet_length,
			UDMA_SIZE_8,
			UDMA_ARB_8, contents,
			UDMA_SRC_INC_8, (void *) (UART_BASE_PTR + UART_O_DR),
			UDMA_DST_INC_NONE);

			UpdateReadPtr(pRxBuffer, RX_PACKET_SIZE);

//			bTXdone = false;

			UARTDMAEnable(UART_BASE_PTR, UART_DMA_TX);

		} else
			UARTDMADisable(UART_BASE_PTR, UART_DMA_TX);
	}

	if (interruptStatus & UART_INT_DMARX) {

		if (uDMAChannelModeGet(
		UDMA_UART_RX_CHANNEL | UDMA_PRI_SELECT) == UDMA_MODE_STOP) {
			TimerDisable(TIMERA0_BASE, TIMER_A);
			transceiverFrame_header.packet_length =
			UART_UDMA_TRANSFER_SIZE;
			memcpy(
					&(g_ucRxBufA[sizeof(transceiverFrame_header)
							- sizeof(transceiverFrame_header.packet_length)]),
					&(transceiverFrame_header.packet_length),
					sizeof(transceiverFrame_header.packet_length));
			sendBuffer = g_ucRxBufA;
			hasDataToSend = true;
//    		HWREG(GPIOA2_BASE + (GPIO_O_GPIO_DATA + (0x40 << 2))) = ~HWREG(GPIOA2_BASE + (GPIO_O_GPIO_DATA + (0x40 << 2)));
//			HWREG(GPIOA3_BASE + (GPIO_O_GPIO_DATA + (0x10 << 2))) |= 0x10;
//    		while(sl_Send(socket_handle, g_ucRxBufA, sizeof(g_ucRxBufA),
//    				SL_RAW_RF_TX_PARAMS(CHANNEL_NUMBER, RATE_54M, POWER_LEVEL_TONE, PREAMBLE)) != sizeof(g_ucRxBufA));
//    		HWREG(GPIOA3_BASE + (GPIO_O_GPIO_DATA + (0x10 << 2))) &= ~0x10;
			UDMASetupTransfer(UDMA_UART_RX_CHANNEL | UDMA_PRI_SELECT,
			UDMA_MODE_PINGPONG,
			UART_UDMA_TRANSFER_SIZE,
			UDMA_SIZE_8,
			UDMA_ARB_8, (void *) (UART_BASE_PTR + UART_O_DR),
			UDMA_SRC_INC_NONE, &(g_ucRxBufA[sizeof(TransceiverFrameHeader)]),
			UDMA_DST_INC_8);
//    		HWREG(GPIOA2_BASE + (GPIO_O_GPIO_DATA + (0x40 << 2))) = ~HWREG(GPIOA2_BASE + (GPIO_O_GPIO_DATA + (0x40 << 2)));
			TimerLoadSet(TIMERA0_BASE, TIMER_A,
					MICROSECONDS_TO_TICKS(TIMER_DELAY_uS));
			TimerEnable(TIMERA0_BASE, TIMER_A);
		} else if (uDMAChannelModeGet(
		UDMA_UART_RX_CHANNEL | UDMA_ALT_SELECT) == UDMA_MODE_STOP) {
			TimerDisable(TIMERA0_BASE, TIMER_A);
			transceiverFrame_header.packet_length =
			UART_UDMA_TRANSFER_SIZE;
			memcpy(
					&(g_ucRxBufB[sizeof(transceiverFrame_header)
							- sizeof(transceiverFrame_header.packet_length)]),
					&(transceiverFrame_header.packet_length),
					sizeof(transceiverFrame_header.packet_length));
			sendBuffer = g_ucRxBufB;
			hasDataToSend = true;
//			HWREG(GPIOA3_BASE + (GPIO_O_GPIO_DATA + (0x10 << 2))) |= 0x10;
//			while(sl_Send(socket_handle, g_ucRxBufB, sizeof(g_ucRxBufB),
//					SL_RAW_RF_TX_PARAMS(CHANNEL_NUMBER, RATE_54M, POWER_LEVEL_TONE, PREAMBLE)) !=(sizeof(g_ucRxBufB)));
//			HWREG(GPIOA3_BASE + (GPIO_O_GPIO_DATA + (0x10 << 2))) &= ~0x10;
			UDMASetupTransfer(UDMA_UART_RX_CHANNEL | UDMA_ALT_SELECT,
			UDMA_MODE_PINGPONG,
			UART_UDMA_TRANSFER_SIZE,
			UDMA_SIZE_8,
			UDMA_ARB_8, (void *) (UART_BASE_PTR + UART_O_DR),
			UDMA_SRC_INC_NONE, &(g_ucRxBufB[sizeof(TransceiverFrameHeader)]),
			UDMA_DST_INC_8);
//			HWREG(GPIOA2_BASE + (GPIO_O_GPIO_DATA + (0x40 << 2))) = ~HWREG(GPIOA2_BASE + (GPIO_O_GPIO_DATA + (0x40 << 2)));
			TimerLoadSet(TIMERA0_BASE, TIMER_A,
					MICROSECONDS_TO_TICKS(TIMER_DELAY_uS));
			TimerEnable(TIMERA0_BASE, TIMER_A);
		}
	}
//	HWREG(GPIOA2_BASE + (GPIO_O_GPIO_DATA + (0x02 << 2))) = ~HWREG(GPIOA2_BASE + (GPIO_O_GPIO_DATA + (0x02 << 2)));

	UARTIntClear(UART_BASE_PTR, interruptStatus);
}

int main() {
	long lRetVal = -1;
	unsigned char policyVal;

	//
	// Initailizing the board
	//
	BoardInit();

	//
	// Muxing for Enabling UART_TX and UART_RX.
	//
	PinMuxConfig();

	GPIO_IF_LedConfigure(LED1 | LED2 | LED3);
	GPIO_IF_LedOff(MCU_ALL_LED_IND);
	HWREG(GPIOA3_BASE + (GPIO_O_GPIO_DATA + (0x10 << 2))) &= ~0x10;

	InitializeAppVariables();

	lRetVal = ConfigureSimpleLinkToDefaultState();
	if (lRetVal < 0) {
		if (DEVICE_NOT_IN_STATION_MODE == lRetVal)
			LOOP_FOREVER()
		;
	}

	CLR_STATUS_BIT_ALL(g_ulStatus);

	lRetVal = sl_Start(0, 0, 0);
	if (lRetVal < 0 || ROLE_STA != lRetVal) {
		LOOP_FOREVER()
		;
	}

	lRetVal = sl_WlanPolicySet(SL_POLICY_CONNECTION,
			SL_CONNECTION_POLICY(0, 0, 0, 0, 0), &policyVal, 1);
	if (lRetVal < 0) {
		LOOP_FOREVER()
		;
	}

	CreateFilters();

	socket_handle = sl_Socket(SL_AF_RF, SL_SOCK_RAW, CHANNEL_NUMBER);
	SlSockNonblocking_t enableOption;
	enableOption.NonblockingEnabled = 1;
	sl_SetSockOpt(socket_handle, SL_SOL_SOCKET, SL_SO_NONBLOCKING,
			(_u8 *) &enableOption, sizeof(enableOption)); // Enable/disable nonblocking mode

	TransceiverFrameHeader *frame_header = NULL;

	pRxBuffer = CreateCircularBuffer(RX_BUFFER_SIZE);
	if (pRxBuffer == NULL) {
//		HWREG(GPIOA1_BASE + (GPIO_O_GPIO_DATA + (0x02 << 2))) |= 0x02;// Turn ON Red LED to sign ERROR
		LOOP_FOREVER()
		;
	}

	memcpy(g_ucRxBufA, &transceiverFrame_header,
			sizeof(transceiverFrame_header));
	memcpy(g_ucRxBufB, &transceiverFrame_header,
			sizeof(transceiverFrame_header));
	memcpy(g_uartFifoBuf, &transceiverFrame_header,
			sizeof(transceiverFrame_header));

	//
	// uDMA Initialization
	//
	UDMAInit();

	UDMAChannelSelect(UDMA_UART_RX_CHANNEL, NULL);
	UDMAChannelSelect(UDMA_UART_TX_CHANNEL, NULL);

#if defined(SERVER) || defined(DUPLEX)
	uDMAIntRegister(UDMA_INT_ERR, uDMAErrorHandler);
#endif

	UDMASetupTransfer(UDMA_UART_RX_CHANNEL | UDMA_PRI_SELECT,
	UDMA_MODE_PINGPONG,
	UART_UDMA_TRANSFER_SIZE,
	UDMA_SIZE_8,
	UDMA_ARB_8, (void *) (UART_BASE_PTR + UART_O_DR),
	UDMA_SRC_INC_NONE, &(g_ucRxBufA[sizeof(TransceiverFrameHeader)]),
	UDMA_DST_INC_8);

	UDMASetupTransfer(UDMA_UART_RX_CHANNEL | UDMA_ALT_SELECT,
	UDMA_MODE_PINGPONG,
	UART_UDMA_TRANSFER_SIZE,
	UDMA_SIZE_8,
	UDMA_ARB_8, (void *) (UART_BASE_PTR + UART_O_DR),
	UDMA_SRC_INC_NONE, &(g_ucRxBufB[sizeof(TransceiverFrameHeader)]),
	UDMA_DST_INC_8);

	//
	// Enable Rx DMA requests from UART
	//
	UARTDMAEnable(UART_BASE_PTR, UART_DMA_RX);

#if defined(SERVER) || defined(DUPLEX)
	//
	// Register interrupt handler for UART
	//
	UARTIntRegister(UART_BASE_PTR, UARTIntHandler);
#endif

	//
	// Enable DMA done interrupts for uart
	//
	UARTIntEnable(UART_BASE_PTR,
			UART_INT_DMARX | UART_INT_DMATX | UART_INT_OE | UART_INT_BE
					| UART_INT_PE | UART_INT_FE);

	UARTConfigSetExpClk(UART_BASE_PTR,
	MAP_PRCMPeripheralClockGet(UART_PRCM),
	UART_BAUD_RATE, (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
	UART_CONFIG_PAR_NONE));

#ifdef USE_UART1
	UARTFlowControlSet(UART_BASE_PTR, UART_FLOWCONTROL_RX | UART_FLOWCONTROL_TX);
#endif

	//
	// Configure the UART Tx and Rx FIFO level to 1/8 i.e 2 characters
	//
	UARTFIFOLevelSet(UART_BASE_PTR, UART_FIFO_TX4_8, UART_FIFO_RX4_8);

	PRCMPeripheralClkEnable(PRCM_TIMERA0, PRCM_RUN_MODE_CLK);
	PRCMPeripheralReset(PRCM_TIMERA0);

	SysTickInit();
	SysTickIntDisable();

#if defined(SERVER) || defined(DUPLEX)
	TimerConfigure(TIMERA0_BASE, TIMER_CFG_PERIODIC);
	TimerLoadSet(TIMERA0_BASE, TIMER_A, MICROSECONDS_TO_TICKS(TIMER_DELAY_uS));
	TimerIntRegister(TIMERA0_BASE, TIMER_A, TimerA0InterruptHandler);
	TimerIntEnable(TIMERA0_BASE, TIMER_TIMA_TIMEOUT);
	TimerEnable(TIMERA0_BASE, TIMER_A);
#endif

	while (true) {

		if (hasDataToSend) {
			frame_header = (TransceiverFrameHeader *) *sendBuffer;
			HWREG(GPIOA3_BASE + (GPIO_O_GPIO_DATA + (0x10 << 2))) |= 0x10;
			//while (
					sl_Send(socket_handle, *sendBuffer,
					frame_header->packet_length + sizeof(TransceiverFrameHeader)
							+ WLAN_FCS,
					SL_RAW_RF_TX_PARAMS(CHANNEL_NUMBER, RATE_54M,
							POWER_LEVEL_TONE, PREAMBLE));
			//<= 0);
			HWREG(GPIOA3_BASE + (GPIO_O_GPIO_DATA + (0x10 << 2))) &= ~0x10;
			hasDataToSend = false;
		}

#if defined(CLIENT) || defined(DUPLEX)

//		if (bTXdone) {
//
//			if (sl_Recv(socket_handle, g_ucTxBuf, sizeof(g_ucTxBuf), 0) >= 0) {
//
//				frame_header =
//						(TransceiverFrameHeader *) &g_ucTxBuf[sizeof(TransceiverRxOverHead_t)];
//
//				unsigned char* contents =
//						&(g_ucTxBuf[sizeof(TransceiverRxOverHead_t)
//								+ sizeof(TransceiverFrameHeader)]);
//
//				UDMASetupTransfer(UDMA_UART_TX_CHANNEL | UDMA_PRI_SELECT,
//				UDMA_MODE_BASIC, frame_header->packet_length,
//				UDMA_SIZE_8,
//				UDMA_ARB_8, contents,
//				UDMA_SRC_INC_8, (void *) (UART_BASE_PTR + UART_O_DR),
//				UDMA_DST_INC_NONE);
//
//				bTXdone = false;
//				MAP_UARTDMAEnable(UART_BASE_PTR, UART_DMA_TX);
//			}
//		}

		if (IsBufferSizeFilled(pRxBuffer, RX_BUFFER_SIZE))
			HWREG(GPIOA1_BASE + (GPIO_O_GPIO_DATA + (0x02 << 2))) |= 0x02;// Turn ON Red LED to sign ERROR

		if (sl_Recv(socket_handle, GetWritePtr(pRxBuffer),
		RX_PACKET_SIZE, 0) >= 0) {
			UpdateWritePtr(pRxBuffer, RX_PACKET_SIZE);
			if (uDMAChannelModeGet(
			UDMA_UART_TX_CHANNEL) == UDMA_MODE_STOP) {
				TransceiverFrameHeader *frame_header = NULL;

				frame_header =
						(TransceiverFrameHeader *) &(pRxBuffer->pucReadPtr[sizeof(TransceiverRxOverHead_t)]);

				unsigned char* contents =
						&(pRxBuffer->pucReadPtr[sizeof(TransceiverRxOverHead_t)
								+ sizeof(TransceiverFrameHeader)]);

				UDMASetupTransfer(
				UDMA_UART_TX_CHANNEL | UDMA_PRI_SELECT,
				UDMA_MODE_BASIC, frame_header->packet_length,
				UDMA_SIZE_8,
				UDMA_ARB_8, contents,
				UDMA_SRC_INC_8, (void *) (UART_BASE_PTR + UART_O_DR),
				UDMA_DST_INC_NONE);

				UpdateReadPtr(pRxBuffer, RX_PACKET_SIZE);

				UARTDMAEnable(UART_BASE_PTR, UART_DMA_TX);
			}

		}

#endif

	}
}
