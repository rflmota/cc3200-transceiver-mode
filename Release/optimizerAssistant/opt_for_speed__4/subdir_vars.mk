################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../cc3200v1p32.cmd 

C_SRCS += \
../circ_buff.c \
C:/TI/CC3200SDK_1.1.0/cc3200-sdk/example/common/gpio_if.c \
../main.c \
../pinmux.c \
C:/TI/CC3200SDK_1.1.0/cc3200-sdk/example/common/startup_ccs.c \
../systick_if.c \
C:/TI/CC3200SDK_1.1.0/cc3200-sdk/example/common/uart_if.c \
../udma_if.c 

OBJS += \
./circ_buff.obj \
./gpio_if.obj \
./main.obj \
./pinmux.obj \
./startup_ccs.obj \
./systick_if.obj \
./uart_if.obj \
./udma_if.obj 

C_DEPS += \
./circ_buff.pp \
./gpio_if.pp \
./main.pp \
./pinmux.pp \
./startup_ccs.pp \
./systick_if.pp \
./uart_if.pp \
./udma_if.pp 

C_DEPS__QUOTED += \
"circ_buff.pp" \
"gpio_if.pp" \
"main.pp" \
"pinmux.pp" \
"startup_ccs.pp" \
"systick_if.pp" \
"uart_if.pp" \
"udma_if.pp" 

OBJS__QUOTED += \
"circ_buff.obj" \
"gpio_if.obj" \
"main.obj" \
"pinmux.obj" \
"startup_ccs.obj" \
"systick_if.obj" \
"uart_if.obj" \
"udma_if.obj" 

C_SRCS__QUOTED += \
"../circ_buff.c" \
"C:/TI/CC3200SDK_1.1.0/cc3200-sdk/example/common/gpio_if.c" \
"../main.c" \
"../pinmux.c" \
"C:/TI/CC3200SDK_1.1.0/cc3200-sdk/example/common/startup_ccs.c" \
"../systick_if.c" \
"C:/TI/CC3200SDK_1.1.0/cc3200-sdk/example/common/uart_if.c" \
"../udma_if.c" 


